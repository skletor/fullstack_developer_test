﻿using FullStackDeveloperTest.Api.Repository;
using FullStackDeveloperTest.Api.Repository.Context;
using System.Linq;
using System.Web.Http;

namespace FullStackDeveloperTest.Api.Controllers
{
    public class ProdutosController : ApiController
    {
        readonly ProdutoRepository _produtoRepository;
        
        public ProdutosController(){
            _produtoRepository = new ProdutoRepository(new FullStackDeveloperTestDataContext());
        }

        [Authorize]//Decorando com o Middleware Authorize para validar o Token pelo OAuth
        [HttpGet]
        public IHttpActionResult Get()
        {
            //Retorna Status 200, e a lista de produtos, se passou pelo Authorize
            return Ok(_produtoRepository.ObterTodos().ToList());
        }
    }
}
