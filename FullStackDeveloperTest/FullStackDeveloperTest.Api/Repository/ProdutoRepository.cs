﻿using FullStackDeveloperTest.Api.Repository.Interfaces;
using System.Collections.Generic;
using FullStackDeveloperTest.Api.Models;
using FullStackDeveloperTest.Api.Repository.Context;
using System.Linq;

namespace FullStackDeveloperTest.Api.Repository
{
    public class ProdutoRepository : IProdutoRepository
    {
        private FullStackDeveloperTestDataContext _context;

        public ProdutoRepository(FullStackDeveloperTestDataContext context){
            _context = new FullStackDeveloperTestDataContext();
        }

        public ICollection<Produto> ObterTodos(){
            return _context.Produtos.ToList();
        }

        public void Dispose(){
            _context.Dispose();
        }
    }
}