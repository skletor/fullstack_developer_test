﻿using FullStackDeveloperTest.Api.Models;
using FullStackDeveloperTest.Api.Repository.Context;
using FullStackDeveloperTest.Api.Repository.Interfaces;
using System.Linq;

namespace FullStackDeveloperTest.Api
{
    public class UsuarioAcessoRepository : IUsuarioAcessoRepository
    {
        private FullStackDeveloperTestDataContext _context;

        public UsuarioAcessoRepository(FullStackDeveloperTestDataContext contexto){
            _context = contexto;
        }

        //Obtem os dados de autenticação no Banco de dados
        public UsuarioAcesso Autenticar(string login, string senha)
        {
            return _context.UsuarioAcesso.Where(x => x.Login.ToLower().Equals(login)
                                                        && x.Senha.Equals(senha)).FirstOrDefault();
        }
       
        public void Dispose(){
            _context.Dispose();
        }
    }
}