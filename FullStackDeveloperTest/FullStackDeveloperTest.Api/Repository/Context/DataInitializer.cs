﻿using FullStackDeveloperTest.Api.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace FullStackDeveloperTest.Api.Repository.Context
{
    public class DataInitializer : DropCreateDatabaseAlways<FullStackDeveloperTestDataContext>
    {
        //Sobrescreve o Seed para iniciar a base com alguns dados
        protected override void Seed(FullStackDeveloperTestDataContext context)
        {
            List<Produto> produtos = new List<Produto>()
            {
                new Produto("The Legend Of Zelda Ocarina Of Time - Remake",
                            "Um Remake para Nintendo Wii U do melhor Zelda já feito de todos os tempos"
                            ,32.50M),
                new Produto("Novas Tencologias",
                            "Livro falando sobre todas as últimas Tencologias do mercado de Desenvolvimento"
                            ,32.50M)
               ,new Produto("Série Dvd Smallville","Assista em sua casa toda a Série do joven Super Boy", 95.50M)
               ,new Produto("Street Fighter V", "Novo Jogo da Capcom revivendo o clássico", 32.50M)
               ,new Produto("Iphone 7", "Apple IPhone 7, 64Gb, Prata", 3500M)
            };

            List<UsuarioAcesso> usuariosComAcesso = new List<UsuarioAcesso>()
            {
                new UsuarioAcesso("Admin", "Admin123"),
                new UsuarioAcesso("Fcamara", "!321@Teste"),
                new UsuarioAcesso("Carlos.m", "!321@Teste"),
                new UsuarioAcesso("carla2017", "!321@Teste"),
            };

            context.Produtos.AddRange(produtos);
            context.UsuarioAcesso.AddRange(usuariosComAcesso);
            
            context.SaveChanges();
        }
    }
}