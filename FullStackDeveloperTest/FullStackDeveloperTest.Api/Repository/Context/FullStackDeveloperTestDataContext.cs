﻿using FullStackDeveloperTest.Api.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace FullStackDeveloperTest.Api.Repository.Context
{
    public class FullStackDeveloperTestDataContext : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<UsuarioAcesso> UsuarioAcesso { get; set; }

        public FullStackDeveloperTestDataContext() : base("FullStackDeveloperTestConnectionString") {
          
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Removendo a pluralização ao criar as tabelas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}