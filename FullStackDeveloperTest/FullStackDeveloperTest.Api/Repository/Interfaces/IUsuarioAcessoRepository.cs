﻿using FullStackDeveloperTest.Api.Models;
using System;

namespace FullStackDeveloperTest.Api.Repository.Interfaces{
    public interface IUsuarioAcessoRepository : IDisposable{
        UsuarioAcesso Autenticar(string login, string senha);
    }
}
