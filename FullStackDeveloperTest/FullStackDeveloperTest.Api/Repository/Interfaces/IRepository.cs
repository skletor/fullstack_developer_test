﻿using System;
using System.Collections.Generic;

namespace FullStackDeveloperTest.Api.Repository.Interfaces{
    public interface IRepository<T> : IDisposable where T : class{
        ICollection<T> ObterTodos();
    }
}
