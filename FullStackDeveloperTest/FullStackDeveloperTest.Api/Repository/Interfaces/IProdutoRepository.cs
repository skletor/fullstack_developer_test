﻿using FullStackDeveloperTest.Api.Models;
using System.Collections.Generic;

namespace FullStackDeveloperTest.Api.Repository.Interfaces{
    public interface IProdutoRepository : IRepository<Produto>{
        new ICollection<Produto> ObterTodos();
    }
}