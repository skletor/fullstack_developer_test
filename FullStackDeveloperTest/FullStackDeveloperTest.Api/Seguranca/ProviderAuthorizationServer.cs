﻿using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using System.Security.Claims;
using FullStackDeveloperTest.Api.Repository.Interfaces;
using FullStackDeveloperTest.Api.Repository.Context;

namespace FullStackDeveloperTest.Api.Seguranca
{
    //Provider de geração de token a partir da validação de dados de acesso
    public class ProviderAuthorizationServer : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context){
           context.Validated();
        }

        //quando acessar o token end point na rota

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context){

            using (IUsuarioAcessoRepository _usuarioRepository = new UsuarioAcessoRepository(new FullStackDeveloperTestDataContext()))
            {
                var user = _usuarioRepository.Autenticar(context.UserName, context.Password);

                if (user == null)
                {
                    context.SetError("invalid_grant", "Usuário ou Senha Incorretos!");
                    return;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("role", "user"));
           
            context.Validated(identity);
        }


        //redefinindo o retorno do end point do Token
        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            if (context.OwinContext.Request.Method == "OPTIONS" && context.IsTokenEndpoint)
            {
                /*normalizando o retorno status 400, gera erro no client Side, após retorno do 
                   HttpRequest */
                context.OwinContext.Response.StatusCode = 200;
                context.RequestCompleted();

                return Task.FromResult<object>(null);
            }

            return base.MatchEndpoint(context);
        }
    }
}