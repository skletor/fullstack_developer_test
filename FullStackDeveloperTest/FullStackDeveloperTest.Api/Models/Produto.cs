﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FullStackDeveloperTest.Api.Models
{
    /// <summary>
    /// Modelo para a implementação dos produtos 
    /// </summary>
    public class Produto
    {
        [Key]
        public int ProdutoId { get; set; }
        public string Nome { get; set; }

        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        [DisplayName("Preço")]
        public decimal Preco { get; set; }

        //Construtor Privado para o Entity Framework
        public Produto() { }

        public Produto(string nome, string descricao, decimal preco)
        {
            Nome = nome;
            Descricao = descricao;
            Preco = preco;
        }
    }
}