﻿using System.ComponentModel.DataAnnotations;

namespace FullStackDeveloperTest.Api.Models
{
    public class UsuarioAcesso
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Login Obrigatório")]
        public string Login { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Login Obrigatório")]
        public string Senha { get; set; }

        //construtor privado para o EF
        private UsuarioAcesso() { }

        public UsuarioAcesso(string login, string senha) {
            Login = login;
            Senha = senha;
        }
    }
}