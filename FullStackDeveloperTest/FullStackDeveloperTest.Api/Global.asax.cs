﻿using FullStackDeveloperTest.Api.Repository.Context;
using System;
using System.Data.Entity;
using System.Web;
using System.Web.Http;

namespace FullStackDeveloperTest.Api
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start(){

            Database.SetInitializer(new DataInitializer());//Inicializando o Banco

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_BeginRequest(object sender, EventArgs e){

            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept");
        }
    }
}
