//encapsulando o escopo
(function(){

    //Criando o controller para o m�dulo de produtos ProdutosModule
    angular.module('ProdutosModule')

    .factory('ProdutosService', ['$rootScope', '$cookieStore', '$http',
      
      function($rootScope, $cookieStore, $http){

          //obtem uma lista de produtos na Api
          function ListarProdutos(){

                return $http.get($rootScope.urlBase + 'api/produtos');
          }
          
          //retorna o service com as fun��es necess�rias para implementarmos uma listagem de produtos
          var service = {
              ListarProdutos: ListarProdutos
          };

          return service;            
      }

    ]);
})();