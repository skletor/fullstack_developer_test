//encapsulando o escopo
(function(){

    'use strict'

    //Registra o Servive de Autenticação para o módulo AutenticacaoModule
    angular.module('AutenticacaoModule')

    .factory('AutenticacaoService', ['$rootScope', '$cookieStore', '$http',
      function($rootScope, $cookieStore, $http){

          //Realiza um post na rota que obtem o token, caso autentique com sucesso
          function LoginServer (userName, password, callback) {

              var data = "grant_type=password&username=" + userName + "&password=" + password;
           
              return $http.post($rootScope.urlBase + 'token', data);
          }

          //caso o login seja efetuado com sucesso, criamos os dados de acesso para guardar o token
          function CriarDadosDeAcesso (username, password, token) {
         
            $rootScope.globals = {
                usuarioAcesso: {
                    token: token
                }
            };
            
            //logado com sucesso, adicionamos o Header Authorization Bearer as requisições do $http
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            
            /*criamos um cookie para guardar os dados de acesso no caso somente o token para as requisições
               aos recursos da Api */
            $cookieStore.put('globals', $rootScope.globals);
          };
          
          //Para implementação de um LogOut na aplicação caso o Token fosse maior que 1 minuto como definido no teste
          function LimparDadosDeAcesso() {
                $rootScope.globals = {};
                $cookieStore.remove('globals');
                $http.defaults.headers.common.Authorization = '';//'Bearer ';
          };
          
          //retorna o servive completo com todas as funções necessárias para controlar a autorização a aplicação
          var service = {
                LoginServer: LoginServer,
                CriarDadosDeAcesso: CriarDadosDeAcesso,
                LimparDadosDeAcesso: LimparDadosDeAcesso 
          };

          return service;
      }
    ]);

})();