//encapsulando o escopo
(function(){
    'use strict'
    
    angular.module('ProdutosModule',[]); //módulo para os produtos
    angular.module('AutenticacaoModule',[]);//módulo para autenticação

    //módulo geral da aplicação
    angular.module('FullStackDeveloperTestApp', [
        'AutenticacaoModule',
        'ProdutosModule',
        'ngRoute',
        'ngCookies'
    ])
    //Configurando as rotas
    .config(['$routeProvider', function($routeProvider){

        $routeProvider
            .when('/loginpage',{
                templateUrl: 'views/loginpage.html',
                controller: 'AutenticacaoController',
                css: ['styles/material.min.css', 'styles/app.css']
            })
            .when('/',{
                templateUrl: 'views/produtos.html',
                controller: 'ProdutosController'
            })
            .otherwise({redirectTo:'/index.html'});
    }])
    //Configurando inicialização da aplicação
    .run(['$rootScope','$location','$cookieStore','$http', '$timeout', 
        function ($rootScope, $location, $cookieStore, $http, $timeout) {
            
            //porta que define onde está executando a Api Rest
            $rootScope.portTestServer = '49968/';//Auterar aqui se necessário

            $rootScope.urlBase = 'http://localhost:' + $rootScope.portTestServer;

            // $http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';   
            // $http.defaults.headers.common['Access-Control-Allow-Headers'] = 'Content-Type, Authorization';
            // $http.defaults.headers.common['Access-Control-Allow-Method'] = 'GET, POST, OPTIONS';
            $http.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';

            //mantem o usuário logado mesmo atualizando a página, obten do cookie ou local storage
            $rootScope.globals = $cookieStore.get('globals') || {};
            if($rootScope.globals.usuarioAcesso){
                $http
                    .defaults
                    .headers
                    .common['Authorization'] = 'Bearer ' + $rootScope.globals.usuarioAcesso.token;
            }

            //Interceptando a mudança de rotas para verificar se existe os dados de acesso com o Token
            //no $locationChangeStart event listener 
            $rootScope.$on('$locationChangeStart', function(event, next, current){
                
                if($location.path() !== '/loginpage' && !$rootScope.globals.usuarioAcesso){
                    $location.path('/loginpage');
                }
            });

            /*atualizando os elementos dinâmicos, como estamos utilizando o ngView
            //isto é necessário para funcionar os binds de animação do material design lite*/
            $rootScope.$on('$viewContentLoaded', function () {
                $timeout(function () {
                    componentHandler.upgradeAllRegistered();
                });
            });
    }]);
})();