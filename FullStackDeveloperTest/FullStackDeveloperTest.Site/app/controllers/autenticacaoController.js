//encapsulando o escopo
(function(){
'use strict'

    //Cria o controller para o Módulo de Autenticação já injetado no módulo principal
    angular.module('AutenticacaoModule')

    .controller('AutenticacaoController', [ '$rootScope', '$scope', '$location', 'AutenticacaoService',
        function($rootScope, $scope, $location, AutenticacaoService){

            //limpa os dados de acesso do usuário
            AutenticacaoService.LimparDadosDeAcesso();

            //Função Logar acionada no formulário de login
            $scope.Logar = function(){
                
                $scope.msgErroAutenticacao = '';
                $rootScope.msgRotaNaoAutorizada = '';

                //evita 1 request ao servidor caso usuário não tenha preenchido os dados de login
                if (($scope.usuarioLogin == '' || $scope.usuarioLogin == undefined || $scope.usuarioLogin == null)
                    || ($scope.senha == '' || $scope.senha == undefined || $scope.senha == null)) {
                        
                    $scope.msgErroAutenticacao = 'Preencha os dados de acesso!';
                    return;
                }                    

                SetaLoadingBar(true);
               
                //Utiliza o servive para realizar a requisição no server
                AutenticacaoService.LoginServer($scope.usuarioLogin, $scope.senha)
                    .then(function (respostaServer) {
                         
                        SetaLoadingBar(false);

                         if (respostaServer.data.access_token !== undefined
                            && respostaServer.data.access_token !== null
                            && respostaServer.data.access_token !== "") {

                              //se gerou um token válido, cria os dados de acesso localmente  
                              AutenticacaoService.CriarDadosDeAcesso($scope.usuarioLogin,
                                                                     $scope.senha,
                                                                     respostaServer.data.access_token);

                              //e redireciona para a página de produtos                                                  
                              $location.path('/');
                         }

                    }, function errorCallBack(erro) {
                        SetaLoadingBar(false);
                        $scope.msgErroAutenticacao = erro.data.error_description; //gera uma mensagem amigável para o usuário
                        $location.path('/loginpage');
                    });
            }
        }]);

    function SetaLoadingBar(deveExibir) {
        if(deveExibir)
            document.getElementById('loading-login').style.display = 'block';
        else
            document.getElementById('loading-login').style.display = 'none';
    }

})();