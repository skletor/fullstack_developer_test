//encapsulando o escopo
(function(){

    'use strict'

    //Cria um controller para o Módulo de Produtos
    angular.module('ProdutosModule')

    .controller('ProdutosController', ['$rootScope','$scope', '$location',
                                        'ProdutosService', 'AutenticacaoService',
         function($rootScope, $scope, $location, ProdutosService, AutenticacaoService){

             //obtem a lista de produtos via ajax
              ProdutosService.ListarProdutos()
                     .then(function successCallback(response) {
                         $scope.listaDeProdutos = response.data;
                         $rootScope.msgRotaNaoAutorizada = '';
                    
                     }, function errorCallback(erro) {
                         //Token Expirado, o envio do Header Authorization já está definido a nível de Aplicação
                         //então redireciona para a página de Login e limpa os dados de acesso;                        
                         AutenticacaoService.LimparDadosDeAcesso();
                         $rootScope.msgRotaNaoAutorizada = 'Por favor realize o Login';
                         $location.path('/loginpage');
                     });
         }
    ]);

})();