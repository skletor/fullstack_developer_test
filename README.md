# Autenticação WebApi com OAuth#

Este pequeno projeto consiste na validação de acesso a rotas de uma Api, que valida um token do cliente para acesso a um recurso da Api, listagem de produtos como implementado em questão, para obter um token válido de acesso o usuário deve logar-se, caso autenticado com sucesso, ele tem acesso a uma listagem de produtos, caso não, ele é redirecionado para para página de login, o token tem duração de um minuto, após expirado não é mais possível acessar os recursos da Api, até que obtenha um novo token válido.



# Tecnologias Utilizadas #

* .Net Framework 4.5
* AspNet Web Api
* OAuth 
* AngularJs
* SqlServer Express

# Como Configurar para testes locais #

**1-** Após clonado ou realizado o Download Zip da aplicação( extrair para um diretório que não seja muito longe da raiz, para não ocasionar problemas de comprimento de caminhos ao abrir a solution posteriormente)

**2-** Após abrir a solution no visual studio, serão abertos dois projetos 'FullStackDeveloperTest.Api'(Api Rest) e 'FullStackDeveloperTest.Site'(Spa Site),
 clique no menu Tools -> NuGet Package Manager -> Manage NuGet Packages for Solution, irá abrir a tela de gerenciamento do NuGet, clique no botão "Restore" para restaurar as dependências do Projeto  WebApi.

**3-** Abra o Web.config para alterar a string de conexão caso necessário
    já está criada para utilizar uma instância do SqlServer Express
    **connectionString="Data Source=.\SQLEXPRESS;Initial        Catalog=FullStackDeveloperTest;Integrated Security=True" providerName="System.Data.SqlClient"****

**4-** Após as dependências restauradas, execute o comando Control + Shift + B, para realizar Build no Projeto.

**5-** Clique com o Botão Direito do mouse na Solution -> Clique em Proprieades -> Clique na opção para selecionar múltiplos projetos para startar, e marque as opções de cada projeto para Start conforme figura logo mais abaixo

**6-** Aperte a tecla F5 e o projeto será iniciado, a api será executada em uma porta e o site será executado em outra porta, acesse a aba que será aberta pelo visual studio e o IIS Express, e teste a aplicação

### Passo a Passo com imagens de como configurar o ambiente ###

## passo 1: apenas extrair arquivos ou clonar diretório ##

## passo 2: Restauração dos pacotes NuGets ##
   
   ![passo para restaurar os pacotes nuget.png](https://bitbucket.org/repo/nzL8Ke/images/230586253-passo%20para%20restaurar%20os%20pacotes%20nuget.png)  

![passo para clicar no botão restore.png](https://bitbucket.org/repo/nzL8Ke/images/1419587436-passo%20para%20clicar%20no%20bot%C3%A3o%20restore.png)

## passo 3: Alteração da connection string caso necessário ##

![alteração connection string.png](https://bitbucket.org/repo/nzL8Ke/images/124487491-altera%C3%A7%C3%A3o%20connection%20string.png)

## passo 4: Build na Solution ##

## passo 5: Definir os dois projetos para iniciarem a execução:

![passo para selecionar multiplos projetos para iniciar.png](https://bitbucket.org/repo/nzL8Ke/images/3199475053-passo%20para%20selecionar%20multiplos%20projetos%20para%20iniciar.png)

## passo 6: Executar a aplicação apertando F5.

# Demonstração Aplicação em execução #

**Página inicial de login**


## Usuários para teste: ##

**Login	   /     Senha**

Admin	  /     Admin123

Fcamara	  /    !321@Teste

Carlos.m   /   !321@Teste

carla2017   /  !321@Teste
 

![page login exemplo.png](https://bitbucket.org/repo/nzL8Ke/images/367508572-page%20login%20exemplo.png)

**Clicando no botão de logar**

![clicando no botão autenticar.png](https://bitbucket.org/repo/nzL8Ke/images/585698215-clicando%20no%20bot%C3%A3o%20autenticar.png)

** Caso autenticado exibe lista de produtos **

![listagem de produtos.png](https://bitbucket.org/repo/nzL8Ke/images/1639350745-listagem%20de%20produtos.png)

** Caso Token Expire redireciona para o Login **

![token expirado.png](https://bitbucket.org/repo/nzL8Ke/images/3562780725-token%20expirado.png)

** USuário ou senha incorretos **

![Usuário ou senha incorretos.png](https://bitbucket.org/repo/nzL8Ke/images/2484273703-Usu%C3%A1rio%20ou%20senha%20incorretos.png)

### Resolução de problemas caso ocorram ###

0- Access-Control-Allow-Origin:
  
Resolução: Limpe o cache do browser em que está testando para que limpe os cabeçalhos de request e response enviados anteriormente, feche e abra
  Observação: Para a aplicação teste já está tratádo o problema de Cors, mas isso é um problema na maior parte das vezes com os navegadores, Google Chrome principalmente, pode se utilizar o fire fox para melhores resultados dos testes.  

1- iis express não startar
  
Resolução: apagar a pasta .vs na raiz do projeto, para que seja limpa as configs  do iis express.

2- portas não disponíveis no pc/notebook que irá para rodar a aplicação

  Resolução: abra as configurações de cada projeto altere as portas para as desejadas, salve para fazer o mapeamento virtual do iis express, 
 *importante: caso seja alterada a porta em que será executada o WebApi, entrar no arquivo app.module.js, localizado no projeto 'FullStackDeveloperTest.Site'/app/app.module.js e alterar a linha 35 : $rootScope.portTestServer = '49968/';//Auterar aqui se necessário

e alterar o valor da variável do rootScope.portTestServer para a mesma que foi alterada a do projeto WebApi.